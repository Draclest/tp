
import { Router, Request, Response } from 'express'
import Storage from '../model/Storage'
import Adapter from './Adapter'
import Order from '../types'

export default class Controler {
   private storage : Storage;
    constructor() {
         this.storage = new Storage("orders");
    }

    public helloWorld(req: Request, res: Response):void{
        res.send('Hello World !')
    }
    public GetFavicon(req: Request, res: Response):void{
        res.status(204)
    }

    public async GetOrders(req: Request, res: Response):Promise<void>{
        const orders:Order[] = await this.storage.getItems();
        orders.map(order => {
            order.delivery.contact = new Adapter()
        })
        res.json(orders);
    }
    public async GetOneOrders(req: Request, res: Response):Promise<void>{
        const id = req.params.id
        const orders = await this.storage.getItems()
        const result = orders.find((order: any) => order.id ===parseInt(id,10))
        if (!result) {
           res.sendStatus(404)
        }
         res.json(result)
    }
    public async AddOrders(req: Request, res: Response):Promise<void>{
        const orders = await this.storage.getItems()
        const alreadyExists =  orders.find((order: any) => order.id === req.body.id);
        if (alreadyExists) {
           res.sendStatus(409)
        }
        orders.push(req.body)
        await this.storage.setItems(orders)
        res.sendStatus(201)
    }
}