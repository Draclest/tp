import {Contact} from '../types';

export default class Adapteur implements Contact{
   public fullname: string;
   public email: string;
   public phone: string;
   public address: string;
   public postalCode: string;
   public city: string;

    constructor(){
        this.fullname = 'Hello There'
        this.email = 'General Kenobi '
        this.phone = 'do you know'
        this.address = 'the story'
        this.postalCode= 'of'
        this.city = ' darth plagueis'
    }
}