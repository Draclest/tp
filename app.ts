import express from 'express'
import path from 'path'
import cookieParser from 'cookie-parser'
import logger from 'morgan'
import indexRouter from './routes/index'
import ordersRouter from './routes/orders'


export default class App {
  
  private app: express.Application;

  constructor(port: number) {
    this.app = express()
    this.app.set('port',port);
    this._registerMiddlewares();
    this._initDefaultData();
    this._registerRoutes();
  }

private _registerRoutes() {
  
    this.app.use('/', indexRouter)
    this.app.use('/orders', ordersRouter)
}

private _registerMiddlewares() {
  this.app.use(logger('dev'))
  this.app.use(express.json())
  this.app.use(cookieParser())

}

private _initDefaultData() {
    this.app.use(express.urlencoded({ extended: false }))
    this.app.use(express.static(path.join(__dirname, 'public')))
    
}

public getApp() {
  return this.app;
}

}