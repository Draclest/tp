#!/usr/bin/env node

import app from './app'
import Server from './Server'
const port = 9000;
const expressApp = new app(port);
const Serveur = new Server(port,expressApp.getApp());

Serveur.start();
