import { Router, Request, Response } from 'express'

import Controler from '../control/controler'


const router = Router()
const controler = new Controler();

router.get('/', async (req: Request, res: Response) => {
controler.helloWorld(req,res)
})

router.get('/favicon.ico', (req: Request, res: Response) => controler.GetFavicon(req,res ))

export default router