import { Router, Request, Response } from 'express'
import Storage from '../model/Storage'
//import storage from 'node-persist'

import Controler from '../control/controler'
const router = Router()
const controler = new Controler();


router.get('/', async (req: Request, res: Response) => {
  controler.GetOrders(req,res);
})

router.get('/:id', async (req: Request, res: Response) => {
  controler.GetOneOrders(req,res);
})

router.post('/', async (req: Request, res: Response) => {
  controler.AddOrders(req,res);
})

export default router