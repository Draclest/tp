import http from 'http'

export default class Server {
  
    private port: number;
    private app: Express.Application;

    constructor(port:number, app:Express.Application) {
        
        this.port = port;
        this.app = app;
    }

 
    private _normalizePort(value: any): number {
        const port = parseInt(value, 10)
        let  tmp;
        if (isNaN(port)) {
          tmp =  value
        }
        if (port >= 0) {
          tmp =  port
        }
        return tmp;

      
    }
    private _onListening(): void {
        console.log(`Listening on ${this.port}`)
    }

    private _onError(error: any): never {
        if (error.syscall !== 'listen') {
            throw error
          }
        
          switch (error.code) {
            case 'EACCES':
              console.error(`${this.port} requires elevated privileges`)
              process.exit(1)
            case 'EADDRINUSE':
              console.error(`${this.port} is already in use`)
              process.exit(1)
            default:
              throw error
          }

        }
    public start(): void{
            this.port = this._normalizePort(process.env.PORT || this.port);
            const server = http.createServer(this.app);
            server.on('listening', () => this._onListening());
            server.listen(this.port);
            server.on('error', (error) => this._onError(error));
    
        }
  
}
